for i in range(4):
    in_name = "summary_type_{}.txt".format(i + 1)
    out_name = "bbox_type_{}.txt".format(i + 1)
    with open(in_name, 'r') as f, open(out_name, 'w') as g:
        lines = f.readlines()
        summary = dict()
        for l in lines:
            tokens = l[:-1].split("\t")
            coordinate = [int(x) for x in tokens[1:]]
            summary[tokens[0]] = coordinate
        g.write("Filename\tCenter_x\tCenter_y\tWidth\tHeight\n")
        for k in summary.keys():
            print(k)
            x0, x1, y0, y1 = summary[k]
            x0, x1 = max(min(x0, x1), 0), min(max(x0, x1), 256)
            y0, y1 = max(min(y0, y1), 0), min(max(y0, y1), 256)
            c_x, c_y = (x0 + x1)/2, (y0 + y1)/2
            w, h = x1 - x0, y1 - y0
            g.write("{}\t{}\t{}\t{}\t{}\n".format(k, c_x, c_y, w, h))
import numpy as np
import pickle
import random
import math


def random_crop(X, size=224):
    X_crop = np.zeros((X.shape[0], size, size, 3))
    max_x0_loc = X.shape[1] - size
    max_y0_loc = X.shape[2] - size
    for i in range(X.shape[0]):
        x0 = random.randint(0, max_x0_loc)
        y0 = random.randint(0, max_y0_loc)
        x1 = x0 + size
        y1 = y0 + size
        X_crop[i] = X[i, x0:x1, y0:y1, :]
    return X_crop


def load_data(train_path, test_path, random_seed, train_crop=False, test_crop=True):
    """
    Load train and test data from pickled files.

    train/val are split 9 to 1 randomly, unless random_seed is specified.
    only the middle 224-by-224 pixels are used, unless train_crop or test_crop is set.
    Inputs:
    - train_path: filepath to a pickle file of dict = {data, label}
    - test_path: filepath to a pickle file of dict = {data, label}
    - random_seed: random seed for train/val split
    - train_crop: if true, a random crop is used for training
    - test_crop: if true, a random crop is used for testing

    """
    random.seed(random_seed)
    with open(train_path, "rb") as f:
        data_dict = pickle.load(f)
        X = data_dict["data"]
        Y = data_dict["label"]
        print((X.shape, Y.shape))

    with open(test_path, "rb") as f:
        data_dict = pickle.load(f)
        X_test = data_dict["data"]
        X_test_name = data_dict["image_name"]

    Y = Y - 1
    X = X.astype("float32")  # add X_test
    if train_crop:
        X = random_crop(X, size=224)
    # X = X[:,16:240,16:240,:]
    print((X.shape, Y.shape))
    X /= 255
    X_test = X_test.astype("float32")
    if test_crop:
        X_test = X_test[:, 16:240, 16:240, :]
    # X_test = random_crop(X_test, size=224)
    X_test /= 255
    Y = keras.utils.to_categorical(Y, 3)

    N_train = math.floor(X.shape[0] * 0.9)
    N_val = X.shape[0] - N_train
    ix = [i for i in range(X.shape[0])]
    random.shuffle(ix)

    train_ix = ix[:N_train]
    val_ix = ix[N_train:]
    # print(val_ix)

    X_train = X[train_ix]
    y_train = Y[train_ix]
    X_val = X[val_ix]
    y_val = Y[val_ix]

    return (X_train, y_train, X_val, y_val, X_test, X_test_name)


def compare_labels(pred_prob, label, num_classes=3):
    pred_prob = np.array(pred_prob)
    pred_label = np.argmax(pred_prob, axis=1)
    label = label.dot(np.array([0, 1, 2]))
    pred_summary = np.zeros((num_classes, num_classes))
    for i in range(num_classes):
        for j in range(num_classes):
            #             print(np.sum((label == i) * (pred_label == j)).shape)
            pred_summary[i, j] = np.sum((label == i) * (pred_label == j))
    pred_summary_total = np.sum(pred_summary, axis=1)
    pred_summary_prob = np.zeros_like(pred_summary)
    for i in range(num_classes):
        pred_summary_prob[i, :] = pred_summary[i, :] * 1.0 / pred_summary_total[i]
    #     pred_summary_prob = pred_summary/np.reshape(pred_summary_total,(1,num_classes))

    return (pred_summary, pred_summary_total, pred_summary_prob)


def select_images(
    images, pred_prob, label, select_label, num_best_images=5, num_worst_images=5
):
    label = label.dot(np.array([0, 1, 2]))
    pred_prob = np.array(pred_prob)
    #     correct_prob = pred_prob[:,label]
    correct_prob = pred_prob[range(pred_prob.shape[0]), list(label.astype(int))]
    #     sort_prob_ix = np.argsort(correct_prob)
    select_prob = correct_prob[label == select_label]
    select_images = images[label == select_label]
    select_sort_prob_ix = np.argsort(select_prob)
    best_images = np.zeros(
        (num_best_images, images.shape[1], images.shape[2], images.shape[3])
    )
    worst_images = np.zeros(
        (num_worst_images, images.shape[1], images.shape[2], images.shape[3])
    )
    best_probs = np.zeros((num_best_images,))
    worst_probs = np.zeros((num_worst_images,))
    for i in range(num_best_images):
        #         best_images[i] = select_images[select_sort_prob_ix[-i]]
        best_images[i] = select_images[select_sort_prob_ix[-i - 1]]
        best_probs[i] = np.round(select_prob[select_sort_prob_ix[-i - 1]], 3)

    for i in range(num_worst_images):
        worst_images[i] = select_images[select_sort_prob_ix[i]]
        worst_probs[i] = np.round(select_prob[select_sort_prob_ix[i]], 3)

    return (best_images, worst_images, best_probs, worst_probs)


def change_channel(img, new_channel=[2, 1, 0], normalize=True):
    #     image = image.astype(np.uint8)
    if normalize:
        img_max, img_min = np.max(img), np.min(img)
        img = 255.0 * (img - img_min) / (img_max - img_min)
    image = img.astype("uint8")
    new_image = np.zeros_like(image)
    for i in range(len(new_channel)):
        new_image[:, :, i] = image[:, :, new_channel[i]]
    return new_image

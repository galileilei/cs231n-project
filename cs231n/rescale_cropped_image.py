import os
import subprocess as sp
import requests
import cv2
import numpy as np

# project wide constant
TRAIN_DATA = "../input/train"
TEST_DATA = "../input/test"
ADDITIONAL_DATA = "../input/additional"
INPUT_GOOGLE_LINK = "0B6ZV3VU6wPsCc0RYZFdhd3lhc28"
OUTPUT_SUMMARY = "../output/summary_type_{}.txt"

##########################################################
def download_file_from_google_drive(id, destination):
    URL = "https://docs.google.com/uc?export=download"

    session = requests.Session()

    response = session.get(URL, params = { 'id' : id }, stream = True)
    token = get_confirm_token(response)

    if token:
        params = { 'id' : id, 'confirm' : token }
        response = session.get(URL, params = params, stream = True)

    save_response_content(response, destination)

def get_confirm_token(response):
    for key, value in response.cookies.items():
        if key.startswith('download_warning'):
            return value

    return None

def save_response_content(response, destination):
    CHUNK_SIZE = 32768

    with open(destination, "wb") as f:
        for chunk in response.iter_content(CHUNK_SIZE):
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)

def download_inputdata():
    dest = "../input.zip"
    if not os.path.isfile(dest):
        download_file_from_google_drive(INPUT_GOOGLE_LINK, dest)
    if not os.path.isdir("../input"):
        result = sp.check_output(["unzip", dest, "-d", ".."])
        print(result)
        
def get_bbox_coordinate(type_id):
    filename = "../output/summary_type_{}.txt".format(type_id)
    summary = dict()
    with open(filename, 'r') as f:
        lines = f.readlines()
        for l in lines:
            tokens = l[:-1].split("\t")
            coordinate = [int(x) for x in tokens[1:]]
            summary[tokens[0]] = coordinate
    return summary

def rescale(img_location, bbox):
    img = cv2.imread(img_location)
    x0, x1, y0, y1 = bbox
    x0, x1 = max(min(x0, x1), 0), min(max(x0, x1), 256)
    y0, y1 = max(min(y0, y1), 0), min(max(y0, y1), 256)
    print((x0, x1, y0, y1))
    crop = img[y0:y1, x0:x1]
    #crop = img[x0:x1, y0:y1]
    dst = img.copy()
    print((type(dst), dst.shape))
    dst = cv2.resize(crop, (256, 256), interpolation=cv2.INTER_CUBIC)
    return dst

def process_by_type(type_id):
    summary = get_bbox_coordinate(type_id)
    if type_id < 4:
        # passed the test
        os.makedirs("../output/train/Type_{}".format(type_id), exist_ok=True)
        for k in summary.keys():
            print("{} = {}".format(k, summary[k]))
            new_image = rescale(k, summary[k])
            cv2.imwrite("../output/train/Type_{}/{}".format(type_id, os.path.basename(k)), new_image)
    elif type_id == 4:
        os.makedirs("../output/test", exist_ok=True)
        for k in summary.keys():
            print("{} = {}".format(k, summary[k]))
            new_image = rescale(k, summary[k])
            cv2.imwrite("../output/test/{}".format(os.path.basename(k)), new_image)

if __name__ == "__main__":
    download_inputdata()
    for i in [1, 2, 3, 4]:
        process_by_type(i)
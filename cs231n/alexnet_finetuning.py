#!/usr/bin/env python
"""
With this script you can finetune AlexNet as provided in the alexnet.py
class on any given dataset. 
Specify the configuration settings at the beginning according to your 
problem.
This script was written for TensorFlow 1.0 and come with a blog post 
you can find here:
  
https://kratzert.github.io/2017/02/24/finetuning-alexnet-with-tensorflow.html

Author: Frederik Kratzert 
contact: f.kratzert(at)gmail.com
"""
import os
import numpy as np
import tensorflow as tf
from datetime import datetime
from alexnet import AlexNet
import pickle
import tflearn
from tflearn.data_utils import shuffle, to_categorical
import math
import random
import string

#import argparser

"""
user input handling
"""
#parser = argparser.ArgumentParser(description="run alexnet in two stages")
#parser.add_argument("-n", "--num_epoch", action="store", help="number of epoches")
#parser.add_argument("--stage_1", action="store", help="number of epoches in stage 1")
#parser.add_argument("--store_every", action="store", help="the frequency of storing checkpoints")
#parser.add_argument("--levels", action="store", help="number of (first few) layers NOT trained in stage 1")

#if __name__ == "__main__":

"""
Configuration settings
"""

# Path to the textfiles for the trainings and validation set

# Learning params
learning_rate = 0.001
momentum = 0.9
num_epochs = 80
stage_1_epochs = 0
stage_2_epochs = num_epochs - stage_1_epochs
batch_size = 32
store_every = 16

# Network params
regularization_strength = 0.001
dropout_rate = 0.5
num_classes = 3
train_layers = ['fc8', 'fc7', 'fc6', 'conv5', 'conv4', 'conv3', 'conv2', 'conv1']

# How often we want to write the tf.summary data to disk
display_step = 1

# Path for tf.summary.FileWriter and to store model checkpoints
# taken from 
def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

run_id = id_generator()
filewriter_path = "pretrained_alex/logs/{}".format(run_id)
checkpoint_path = "pretrained_alex/ckpt/{}".format(run_id)
os.makedirs(filewriter_path, exist_ok=True)
os.makedirs(checkpoint_path, exist_ok=True)

# Create all path if it doesn't exist
# TODO: may need to change this
def preprocess_alexnet(run_id, path="pretrained_alex"):
    # make the directories
    import os
    import subprocess as sp
    for name in ["weights", "logs/{}/stage_1".format(run_id), "logs/{}/stage_2".format(run_id), "logs/{}/validation".format(run_id), "ckpt/{}".format(run_id)]:
        os.makedirs("{}/{}".format(path, name), exist_ok=True)
    # downloads the weights
    weight_dest = "{}/weights/bvlc_alexnet.npy".format(path)
    if os.path.isfile(weight_dest):
        return
    result = sp.check_output(['wget', 'http://www.cs.toronto.edu/~guerzhoy/tf_alexnet/bvlc_alexnet.npy', '-o', weight_dest])

preprocess_alexnet(run_id)
#if not os.path.isdir(checkpoint_path): os.mkdir(checkpoint_path)

# TF placeholder for graph input and output
x = tf.placeholder(tf.float32, [batch_size, 256, 256, 3])
y = tf.placeholder(tf.float32, [None, num_classes])
keep_prob = tf.placeholder(tf.float32)

# Initialize model
model = AlexNet(x, keep_prob, num_classes, train_layers, weights_path='pretrained_alex/weights/bvlc_alexnet.npy')
# Link variable to model output
score = model.fc8

# List of trainable variables of the layers we want to train
def get_variables(train_layers):
    fc_vars, conv_vars = list(), list()
    for var in tf.trainable_variables():
        found = False
        for prefix in train_layers:
            if not found and var.name.startswith(prefix):
                conv_vars.append(var)
                found = True
    return conv_vars

conv_vars = get_variables(train_layers)

for var in conv_vars:
    print(var.name)
    print(var.get_shape())

# Op for calculating the loss
with tf.name_scope("cross_ent"):
    model_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits = score, labels = y))
    l2_loss = 0.0
    for var in tf.trainable_variables():
        l2_loss += tf.nn.l2_loss(var)
    loss = model_loss + l2_loss

# Evaluation op: Accuracy of the model
with tf.name_scope("accuracy"):
    correct_pred = tf.equal(tf.argmax(score, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Train op
with tf.name_scope("train"):
    # Get gradients of all trainable variables
    conv_grads = tf.gradients(loss, conv_vars)
    full_grads = tf.gradients(loss, tf.trainable_variables())
    conv_grads_and_vars = list(zip(conv_grads, conv_vars))
    full_grads_and_vars = list(zip(full_grads, tf.trainable_variables()))

    # Create optimizer and apply gradient descent to the trainable variables
    #optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    #optimizer = tf.train.MomentumOptimizer(learning_rate, momentum)
    conv_optimizer = tf.train.RMSPropOptimizer(learning_rate, decay=0.9)
    full_optimizer = tf.train.RMSPropOptimizer(learning_rate * 0.1, decay=0.9)
    conv_train_op = conv_optimizer.apply_gradients(grads_and_vars=conv_grads_and_vars)
    full_train_op = full_optimizer.apply_gradients(grads_and_vars=full_grads_and_vars)

# define three different summary writer
def get_summary_writer(grads_and_vars, scalars, name):
    ops = list()
    for g, v in grads_and_vars:
        #temp = tf.summary.histogram(g.name, g)
        #ops.append(temp)
        #tmep = tf.summary.histogram(v.name, v)
        #ops.append(temp)
        pass
    for s in scalars:
        temp = tf.summary.scalar(s.name, s)
        ops.append(temp)
    summary_op = tf.summary.merge(ops)
    writer = tf.summary.FileWriter(os.path.join(filewriter_path, name))
    return summary_op, writer

summary_op_1, stage_1_writer = get_summary_writer(conv_grads_and_vars, [loss, accuracy], "stage_1")
summary_op_2, stage_2_writer = get_summary_writer(full_grads_and_vars, [loss, accuracy], "stage_2")
summary_op_3, validation_writer = get_summary_writer([], [loss, accuracy], "validation")

# Initialize an saver for store model checkpoints
saver = tf.train.Saver()

# get data in here
with open("../datasets/train_batch", "rb") as f:
#with open("../datasets/train_batch", "rb") as f:
    data_dict = pickle.load(f)
#     X = data_dict['data'].transpose([0, 3, 1, 2])
    X = data_dict['data']
    Y = data_dict['label']
    print((X.shape, Y.shape))

Y = Y - 1
X, Y = shuffle(X, Y)
Y = to_categorical(Y, 3)   

N_train = math.floor(X.shape[0] * 0.8)
N_val = X.shape[0] - N_train
ix = [i for i in range(X.shape[0])]
random.shuffle(ix)

train_ix = ix[:N_train]
val_ix = ix[N_train:]
# print(val_ix)

X_train = X[train_ix]
y_train = Y[train_ix]
X_val = X[val_ix]
y_val = Y[val_ix]

# split input stream in batches
train_batches_per_epoch = math.floor(N_train/batch_size)
val_batches_per_epoch = math.floor(N_val/batch_size)
    
# Start Tensorflow session
with tf.Session() as sess:
    # Initialize all variables
    sess.run(tf.global_variables_initializer())

    # Add the model graph to TensorBoard
    #writer.add_graph(sess.graph)

    # Load the pretrained weights into the non-trainable layer
    model.load_initial_weights(sess)

    print("{} Start training...".format(datetime.now()))
    print("{} Open Tensorboard at --logdir {}".format(datetime.now(), 
                                                      filewriter_path))

    # Loop over number of epochs
    for epoch in range(num_epochs):
        print("{} Epoch number: {}".format(datetime.now(), epoch+1))

        step = 0
        train_op, train_writer, train_summary_op = None, None, None
        if epoch < stage_1_epochs:
            train_op = conv_train_op
            train_writer = stage_1_writer
            train_summary_op = summary_op_1
        else:
            train_op = full_train_op
            train_writer = stage_2_writer
            train_summary_op = summary_op_2
            
        while step < train_batches_per_epoch:
            #print("step is {}".format(step))
            # Get a batch of images and labels
            start, end = step * batch_size, np.minimum((step + 1) * batch_size, N_train)
            #print("start, end are{} {}".format(start, end))
            batch_xs, batch_ys = X_train[start:end, :, :, :], y_train[start:end, :]
            #batch_xs, batch_ys = train_generator.next_batch(batch_size)

            _, l = sess.run([train_op, loss], feed_dict={x: batch_xs,
                                          y: batch_ys,
                                          keep_prob: dropout_rate})
            #print("step is {}: curent batch loss is {}".format(step, l))
            # Generate summary with the current batch of data and write to file
            if step%display_step == 0:
                s = sess.run(train_summary_op, feed_dict={x: batch_xs, 
                                                          y: batch_ys, 
                                                          keep_prob: 1.})
                train_writer.add_summary(s, epoch * train_batches_per_epoch + step)
            step += 1

        # Validate the model on the entire validation set
        print("{} Start validation".format(datetime.now()))
        test_acc = 0.
        test_count = 0
        
        for j in range(val_batches_per_epoch):
            # generate validation batches
            #print("validation step is {}".format(j))
            start, end = j * batch_size, np.minimum((j + 1) * batch_size, N_val)
            batch_tx, batch_ty = X_val[start:end], y_val[start:end]
            #batch_tx, batch_ty = val_generator.next_batch(batch_size)
            s, acc = sess.run([summary_op_3, accuracy], feed_dict={x: batch_tx, y: batch_ty, keep_prob: 1.})
            test_acc += acc
            test_count += 1
        test_acc /= test_count
        validation_writer.add_summary(s, epoch)
        print("{} Validation Accuracy = {:.4f}".format(datetime.now(), test_acc))

        # Reset the file pointer of the image data generator
        #val_generator.reset_pointer()
        #train_generator.reset_pointer()

        #save checkpoint of the model
        if epoch%store_every == 0:
            print("{} Saving checkpoint of model...".format(datetime.now())) 
            checkpoint_name = os.path.join(checkpoint_path, 'model_epoch'+str(epoch+1)+'.ckpt')
            save_path = saver.save(sess, checkpoint_name)  
            print("{} Model checkpoint saved at {}".format(datetime.now(), checkpoint_name))
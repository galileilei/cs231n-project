import numpy as np
import random
import math

import keras
from tflearn.data_utils import shuffle
from keras.preprocessing.image import ImageDataGenerator
from keras.layers import Conv2D, MaxPooling2D
from keras.applications.resnet50 import ResNet50
from keras.models import Model, Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers.normalization import BatchNormalization
from keras.models import load_model
from keras.callbacks import (
    ModelCheckpoint,
    LearningRateScheduler,
    TensorBoard,
    EarlyStopping,
)
from keras import regularizers


def random_crop(X, size=224):
    X_crop = np.zeros((X.shape[0], size, size, 3))
    max_x0_loc = X.shape[1] - size
    max_y0_loc = X.shape[2] - size
    for i in range(X.shape[0]):
        x0 = random.randint(0, max_x0_loc)
        y0 = random.randint(0, max_y0_loc)
        x1 = x0 + size
        y1 = y0 + size
        X_crop[i] = X[i, x0:x1, y0:y1, :]
    return X_crop


def append_loss_history(loss_hist, new_loss_hist):
    # both loss_hist and new_loss_hist are dict
    for k, v in loss_hist.items():
        loss_hist[k] = v + new_loss_hist[k]
    return loss_hist


def generate_learning_rate(lr1, lr2, num_lr):
    # generate num_lr learning rates between lr1 and lr2
    min_lr = min(lr1, lr2)
    max_lr = max(lr1, lr2)
    max_scale = max_lr / min_lr
    lr = [min_lr * random.uniform(1, max_scale) for _ in range(num_lr)]
    return lr


class Resnet:
    def __init__(
        self,
        lr,
        size,
        num_classes,
        run_id=0,
        fc_layers=None,
        pretrain=False,
        optimizer_name="rmsprop",
    ):
        # use pretrain weight or not
        # fc_layers is a list of list, [[fc_size, l2_reg, dropout_prob], [fc_size, l2_reg, dropout_prob], ...]
        if pretrain:
            base_model = ResNet50(
                include_top=False, weights="imagenet", input_shape=(size, size, 3)
            )
        else:
            base_model = ResNet50(
                include_top=False, weights=None, input_shape=(size, size, 3)
            )

        x = Flatten()(base_model.output)

        if fc_layers is not None:
            for i in range(len(fc_layers)):
                x = Dense(
                    fc_layers[i][0],
                    activation="relu",
                    kernel_regularizer=regularizers.l2(fc_layers[i][1]),
                )(x)
                if fc_layers[i][2] != 1:
                    x = Dropout(fc_layers[i][2])(x)

        predictions = Dense(num_classes, activation="softmax")(x)

        # create graph of your new model
        self.model = Model(input=base_model.input, output=predictions)

        if optimizer_name == "sgd":
            optimizer = keras.optimizers.SGD(lr=lr, decay=0.1)
        elif optimizer_name == "Adam":
            optimizer = keras.optimizers.Adam(lr=lr)
        else:
            optimizer = keras.optimizers.RMSprop(lr=lr)

        self.model.compile(
            optimizer=optimizer, loss="categorical_crossentropy", metrics=["accuracy"]
        )

        self.datagen = ImageDataGenerator(
            featurewise_center=True,  # set input mean to 0 over the dataset
            samplewise_center=False,  # set each sample mean to 0
            featurewise_std_normalization=False,  # divide inputs by std of the dataset
            samplewise_std_normalization=False,  # divide each input by its std
            zca_whitening=False,  # apply ZCA whitening
            rotation_range=45,  # randomly rotate images in the range (degrees, 0 to 180)
            width_shift_range=0,  # randomly shift images horizontally (fraction of total width)
            height_shift_range=0,  # randomly shift images vertically (fraction of total height)
            horizontal_flip=True,  # randomly flip images
            vertical_flip=False,
        )  # randomly flip images

        self.loss_hist = None

        # Save the model according to the conditions
        self.checkpoint = ModelCheckpoint(
            "resnet_keras/resnet_" + str(run_id) + ".h5",
            monitor="val_acc",
            verbose=1,
            save_best_only=True,
            save_weights_only=False,
            mode="auto",
            period=1,
        )
        self.early = EarlyStopping(
            monitor="val_acc", min_delta=0, patience=10, verbose=1, mode="auto"
        )
        self.tbCallBack = keras.callbacks.TensorBoard(
            log_dir="resnet_keras/Graph_" + str(run_id),
            histogram_freq=0,
            write_graph=True,
            write_images=True,
        )

    def train(self, X_train, y_train, X_val, y_val, epochs, batch_size=64):
        self.datagen.fit(X_train)
        new_loss_hist = self.model.fit_generator(
            self.datagen.flow(X_train, y_train, batch_size=batch_size),
            steps_per_epoch=X_train.shape[0] // batch_size,
            epochs=epochs,
            validation_data=(X_val, y_val),
            callbacks=[self.checkpoint, self.early, self.tbCallBack],
        )

        if self.loss_hist is None:
            self.loss_hist = new_loss_hist.history
        else:
            self.loss_hist = append_loss_history(self.loss_hist, new_loss_hist.history)

    def predict(self, X_test):
        self.model.load_weights("resnet_keras/resnet_" + str(run_id) + ".h5")
        y_test_pred = model.predict(X_test)
        return y_test_pred

    def train_with_random_crop(
        self, X_train, y_train, X_val, y_val, epochs, num_crops, batch_size=64
    ):
        X_val = random_crop(X_val)
        for i in range(num_crops):
            X_train = random_crop(X_train)
            self.train(X_train, y_train, X_val, y_val, epochs, batch_size=batch_size)

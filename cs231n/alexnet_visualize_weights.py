#!/usr/bin/env python
"""
visualize weights from checkpoint files
"""
import os
import numpy as np
import tensorflow as tf
from alexnet import AlexNet
import tflearn
from tflearn.data_utils import shuffle, to_categorical
import math
import random
from vis_utils import visualize_grid
import matplotlib.pyplot as plt
plt.switch_backend("agg")

"""
user input
"""

run_id = "J2Y4DW"
list_of_epochs = [0, 16, 32, 48, 64]
"""
define model
"""
batch_size = 64
x = tf.placeholder(tf.float32, [batch_size, 256, 256, 3])
keep_prob = 0.5
num_classes = 3
train_layers = []
checkpoint_path = "pretrained_alex/ckpt/{}".format(run_id)
model = AlexNet(x, keep_prob, num_classes, train_layers, weights_path='pretrained_alex/weights/bvlc_alexnet.npy')
saver = tf.train.Saver()
num_epoch = 10

def plot_weight(weight, name, output_path = "../results/visualization"):
    os.makedirs(output_path, exist_ok=True)
    w = np.transpose(weight, axes=[3, 0, 1, 2])
    grid = visualize_grid(w)
    fig, ax = plt.subplots()
    ax.imshow(grid.astype(np.uint8))
    fig.set_size_inches(4, 4)
    fig.set_dpi(800)
    plt.axis('off')
    file_name = name.replace("/", "_")
    plt.savefig("{}.png".format(os.path.join(output_path, file_name)), format='png', transparent=True, bbox_inches='tight')
    
if __name__ == "__main__":
    # show original layer 1 image
    weights_dict = np.load(model.WEIGHTS_PATH, encoding = 'bytes').item()
    for op_name in weights_dict:
        if op_name.startswith("conv1"):
            for w in weights_dict[op_name]:
                if len(w.shape) == 4:
                    print((op_name, w.shape))
                    plot_weight(w, op_name)
    
    # show transfer learning layer 1 image
    for epoch in list_of_epochs:
        with tf.Session() as sess:
            checkpoint_name = os.path.join(checkpoint_path, 'model_epoch'+str(epoch+1)+'.ckpt')
            saver.restore(sess, checkpoint_name)
            
            for var in tf.trainable_variables():
                if var.name.startswith("conv1") and len(var.get_shape().as_list()) == 4:
                    weight = sess.run(var)
                    print((epoch, var.name))
                    plot_weight(weight, "{}_{}_{}".format(run_id, epoch, var.name))
import os
import numpy as np
import tensorflow as tf
from datetime import datetime
from alexnet import AlexNet
import pickle
import tflearn
from tflearn.data_utils import shuffle, to_categorical
import math
import random
#from datagenerator import ImageDataGenerator

"""
Configuration settings
"""
run_id = "J2Y4DW"
list_of_epoch = [0, 16, 32, 48, 64]

# Learning params
learning_rate = 0.001
momentum = 0.9
num_epochs = 10
stage_1_epochs = 4
stage_2_epochs = num_epochs - stage_1_epochs
batch_size = 32

# Network params
regularization_strength = 0.01
dropout_rate = 0.5
num_classes = 3
train_layers = ['fc8', 'fc7', 'fc6', 'conv5', 'conv4']

# How often we want to write the tf.summary data to disk
display_step = 1

# Path for tf.summary.FileWriter and to store model checkpoints
filewriter_path = "pretrained_alex/logs/{}".format(run_id)
checkpoint_path = "pretrained_alex/ckpt/{}".format(run_id)

# TF placeholder for graph input and output
x = tf.placeholder(tf.float32, [batch_size, 256, 256, 3])
y = tf.placeholder(tf.float32, [None, num_classes])
keep_prob = tf.placeholder(tf.float32)

# Initialize model
model = AlexNet(x, keep_prob, num_classes, train_layers, weights_path='pretrained_alex/weights/bvlc_alexnet.npy')
# Link variable to model output
score = model.fc8

# List of trainable variables of the layers we want to train
def get_variables(train_layers):
    fc_vars, conv_vars = list(), list()
    for var in tf.trainable_variables():
        found = False
        for prefix in train_layers:
            if not found and var.name.startswith(prefix):
                conv_vars.append(var)
                found = True
    return conv_vars

conv_vars = get_variables(train_layers)

for var in conv_vars:
    print(var.name)
    print(var.get_shape())

# Op for calculating the loss
with tf.name_scope("cross_ent"):
    model_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits = score, labels = y))
    l2_loss = 0.0
    for var in tf.trainable_variables():
        l2_loss += tf.nn.l2_loss(var)
    loss = model_loss + l2_loss

# Evaluation op: Accuracy of the model
with tf.name_scope("accuracy"):
    correct_pred = tf.equal(tf.argmax(score, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Train op
with tf.name_scope("train"):
    # Get gradients of all trainable variables
    conv_grads = tf.gradients(loss, conv_vars)
    full_grads = tf.gradients(loss, tf.trainable_variables())
    conv_grads_and_vars = list(zip(conv_grads, conv_vars))
    full_grads_and_vars = list(zip(full_grads, tf.trainable_variables()))

    # Create optimizer and apply gradient descent to the trainable variables
    #optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    #optimizer = tf.train.MomentumOptimizer(learning_rate, momentum)
    optimizer = tf.train.RMSPropOptimizer(learning_rate, decay=0.9)
    conv_train_op = optimizer.apply_gradients(grads_and_vars=conv_grads_and_vars)
    full_train_op = optimizer.apply_gradients(grads_and_vars=full_grads_and_vars)

# define three different summary writer
def get_summary_writer(grads_and_vars, scalars, name):
    ops = list()
    for g, v in grads_and_vars:
        #temp = tf.summary.histogram(g.name, g)
        #ops.append(temp)
        #tmep = tf.summary.histogram(v.name, v)
        #ops.append(temp)
        pass
    for s in scalars:
        temp = tf.summary.scalar(s.name, s)
        ops.append(temp)
    summary_op = tf.summary.merge(ops)
    writer = tf.summary.FileWriter(os.path.join(filewriter_path, name))
    return summary_op, writer

summary_op_1, stage_1_writer = get_summary_writer(conv_grads_and_vars, [loss, accuracy], "stage_1")
summary_op_2, stage_2_writer = get_summary_writer(full_grads_and_vars, [loss, accuracy], "stage_2")
summary_op_3, validation_writer = get_summary_writer([], [loss, accuracy], "validation")
# add logits
logits = tf.nn.softmax(score)

# Initialize an saver for store model checkpoints
saver = tf.train.Saver()
epoch = list_of_epoch[-1]
with tf.Session() as sess:
    checkpoint_name = os.path.join(checkpoint_path, 'model_epoch'+str(epoch+1)+'.ckpt')
    saver.restore(sess, checkpoint_name)
    # start prediction
    with open("../datasets/test_batch", "rb") as f:
        data_dict = pickle.load(f)
        X_test = data_dict['data']
        X_test_name = data_dict['image_name']

        N_test = X_test.shape[0]
        y_predict = np.zeros((N_test, 3))
        test_batches_per_epoch = math.floor(N_test/batch_size)
        for i in range(test_batches_per_epoch):
            start, end = i * batch_size, np.minimum((i + 1) * batch_size, N_test)
            xs = X_test[start:end]
            y_predict[start:end] = sess.run(logits, feed_dict={x:xs, keep_prob: 1.})

    with open('../results/alexnet_{}.csv'.format(run_id),'w') as f:
        f.write("image_name,Type_1,Type_2,Type_3\n")
        for i in range(y_predict.shape[0]):
            f.write("%s,%f,%f,%f\n" % (X_test_name[i], y_predict[i, 0], y_predict[i, 1], y_predict[i, 2]))
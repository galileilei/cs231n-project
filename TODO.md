# Agenda
===
Two main reasons to revisit this 4 year old repo:
- make this reproducible. Anyone should be able to download this and reproduce
  the work.
- upgrade this to tensorflow 2 or pytorch. 

__05 Oct 2020__
* merge output and results
* separate alexnet, resnet and vgg10
* go through all the notebook
* figure out all the data file location
* figure out where ROI is used

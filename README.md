# Introduction

Application of VGG, Resnet and AlexNet on the task of classification of cervix
types. For a detailed report, see [this link]().

# Challenges
- Highly homogenous/similar classes of images
- Badly taken photographs

# Fold Organization
```bash
.
├── ./cs231n            # source for various models 
├── ./load_data         # download, label input data
├── ./notebook          # experiement/visualization
├── ./output            # results of 
├── ./README.md         # this doucument
├── ./results           # results of
├── ./script            # temporarily empty
└── ./setup.py          # make this a module
```

# Acknowledgement

This was a group final project done by Ruoxuan Xiong, Huiyang and Lei Lei for
CS231N.

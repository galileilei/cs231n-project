#!/usr/bin/env python
"""
download from google drive, see this SO answer:
http://stackoverflow.com/a/39225039/1362569
"""
import requests
import os.path


def download_file_from_google_drive(id, destination):
    URL = "https://docs.google.com/uc?export=download"

    session = requests.Session()

    response = session.get(URL, params={"id": id}, stream=True)
    token = get_confirm_token(response)

    if token:
        params = {"id": id, "confirm": token}
        response = session.get(URL, params=params, stream=True)

    save_response_content(response, destination)


def get_confirm_token(response):
    for key, value in response.cookies.items():
        if key.startswith("download_warning"):
            return value

    return None


def save_response_content(response, destination):
    CHUNK_SIZE = 32768

    with open(destination, "wb") as f:
        for chunk in response.iter_content(CHUNK_SIZE):
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)


def download_file(link, dest):
    if not os.path.isfile(dest):
        download_file_from_google_drive(link, dest)


def main():
    sample_train_batch_link = "0B6CBWgaJr8F-eDlKQV9UTkVjam8"
    download_file(sample_train_batch_link, "sample_train_batch")
    train_batch_link = "0B6CBWgaJr8F-SW9RZ0VjQ1pOLUE"
    download_file(train_batch_link, "train_batch")
    test_batch_link = "0B6ZV3VU6wPsCckx4U21kSEw3NDA"
    download_file(test_batch_link, "test_batch")

    # 8 additional training set batch, 500 each
    # use new google account link
    additional_ids = dict()
    additional_ids[2] = "0B6ZV3VU6wPsCeTVuN3lIZ3RxcHc"
    additional_ids[4] = "0B6ZV3VU6wPsCVVdBMkVlZEF0eEE"
    additional_ids[6] = "0B6ZV3VU6wPsCYWhPSzJnLTJLeEk"
    additional_ids[5] = "0B6ZV3VU6wPsCMEJSS2ROSGVBSXc"
    additional_ids[1] = "0B6ZV3VU6wPsCRnoxbjVnSC1SdzA"
    additional_ids[0] = "0B6ZV3VU6wPsCWGtsaTQxd3NIV00"
    additional_ids[3] = "0B6ZV3VU6wPsCTHNUQWx5VE0tVms"
    additional_ids[7] = "0B6ZV3VU6wPsCQ2dreXExOEplU0U"
    """
    for i in range(8):
        link = additional_ids[i]
        dest = "additional_train_batch_%d" % i
        download_file(link, dest)
    """
    # cropped dataset
    scaled_train_batch_link = "0B6ZV3VU6wPsCZjNXM1hOaDNBX0E"
    download_file(scaled_train_batch_link, "scaled_test_batch")
    scaled_test_batch_link = "0B6ZV3VU6wPsCd09Ybzk4emU2dU0"
    download_file(scaled_test_batch_link, "scaled_train_batch")


if __name__ == "__main__":
    main()

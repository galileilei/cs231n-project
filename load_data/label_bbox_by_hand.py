#!/usr/bin/env python

#####################################################
# to work on class of type 2:
# ./ROI.py -t 1
# Usage:
# 1. click and drag to select ROI.
# 2. press <C> to confirm current selection; if nothing is selected,
# the entire window is used.
# 3. press <R> to redraw the window, clear ROI you just draw.
# 4. press <Q> to quit; this will save some results for you to resume next time.
# Warning:
# 1. if you did not quit this bt press <Q>, nothing is saved.
# 2. you can select ROI larger than current window, dun do that.
#####################################################

import cv2

# from skimage import data
import os
import sys
from glob import glob
import numpy as np
import subprocess as sp
import requests
import argparse

TRAIN_DATA = "../input/train"
TEST_DATA = "../input/test"
ADDITIONAL_DATA = "../input/additional"
INPUT_GOOGLE_LINK = "0B6ZV3VU6wPsCc0RYZFdhd3lhc28"
OUTPUT_SUMMARY = "../output/summary_type_{}.txt"

#####################################################
def define_ROI(image):
    """
    Define a rectangular window by click and drag your mouse.
    Returns a 4 tuple that specify the Region of Interest

    Parameters
    ----------
    image: Input image.
    """

    clone = image.copy()
    rect_pts = []  # Starting and ending points
    x0, y0, x1, y1 = 0, 0, 256, 256
    win_name = "<r> to redraw ROI, <c> to confirm, <ESC> to exit"  # Window name
    start_draw = False
    done = False

    def select_points(event, x, y, flags, param):
        # callback to handle mouse event
        nonlocal x0, x1, y0, y1, start_draw, win_name, image, clone
        if event == cv2.EVENT_LBUTTONDOWN:
            if not start_draw:
                # print("left button pressed {}{}, clicked is true\n".format(x, y))
                x0, y0, start_draw = x, y, True
            else:
                # print("left button pressed {}{}, clicked is false\n".format(x, y))
                x1, y1, start_draw = x, y, False

        elif event == cv2.EVENT_MOUSEMOVE and start_draw:
            # print("mouse moved to {}{}, clicked is true\n".format(x, y))
            clone = image.copy()
            cv2.rectangle(clone, (x0, y0), (x, y), (0, 255, 0), 1)
            cv2.imshow(win_name, clone)

        elif event == cv2.EVENT_LBUTTONUP and start_draw:
            # print("left mouse lifted {}{}, clicked is false\n".format(x, y))
            x1, y1 = x, y
            cv2.rectangle(clone, (x0, y0), (x1, y1), (0, 255, 0), 2)
            cv2.imshow(win_name, clone)
            start_draw = False

    cv2.namedWindow(win_name)
    cv2.setMouseCallback(win_name, select_points)

    while True:
        # display the image and wait for a keypress
        cv2.imshow(win_name, clone)
        key = cv2.waitKey(0) & 0xFF

        if key == ord("r"):  # Hit 'r' to replot the image
            clone = image.copy()

        elif key == ord("c"):  # Hit 'c' to confirm the selection
            break

        elif key == ord("q"):  # Hit 's' to skip the selection
            done = True
            break

    # close the open windows
    cv2.destroyWindow(win_name)

    return done, x0, x1, y0, y1


#####################################
def download_file_from_google_drive(id, destination):
    URL = "https://docs.google.com/uc?export=download"

    session = requests.Session()

    response = session.get(URL, params={"id": id}, stream=True)
    token = get_confirm_token(response)

    if token:
        params = {"id": id, "confirm": token}
        response = session.get(URL, params=params, stream=True)

    save_response_content(response, destination)


def get_confirm_token(response):
    for key, value in response.cookies.items():
        if key.startswith("download_warning"):
            return value

    return None


def save_response_content(response, destination):
    CHUNK_SIZE = 32768

    with open(destination, "wb") as f:
        for chunk in response.iter_content(CHUNK_SIZE):
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)


def download_inputdata():
    dest = "../input.zip"
    if not os.path.isfile(dest):
        download_file_from_google_drive(INPUT_GOOGLE_LINK, dest)
    if not os.path.isdir("../input"):
        result = sp.check_output(["unzip", dest, "-d", ".."])
        print(result)


def load_thumbnails():
    global TRAIN_DATA, TEST_DATA, ADDITIONAL_DATA
    ids = dict()
    files = dict()
    for i in [1, 2, 3]:
        type_info = "Type_{}".format(i)
        k = "train_type_" + str(i)
        print((type_info, k))
        print(os.path.join(TRAIN_DATA, type_info, "*.jpeg"))
        files[k] = glob(os.path.join(TRAIN_DATA, type_info, "*.jpeg"))
        print(files[k])
        ids[k] = np.array(
            [s[len(os.path.join(TRAIN_DATA, type_info)) + 1 : -5] for s in files[k]]
        )

    for k in ids.keys():
        print("{} has {} files".format(k, len(ids[k])))

    files["test"] = glob(os.path.join(TEST_DATA, "*.jpeg"))
    ids["test"] = np.array([s[len(TEST_DATA) + 1 : -5] for s in files["test"]])

    print("{} has {} files".format("test", ids["test"]))

    for i in [1, 2, 3]:
        type_info = "Type_{}".format(i)
        k = "additional_type_" + str(i)
        files[k] = glob(os.path.join(ADDITIONAL_DATA, type_info, "*.jpeg"))
        ids[k] = np.array(
            [
                s[len(os.path.join(ADDITIONAL_DATA, type_info)) + 1 : -5]
                for s in files[k]
            ]
        )

    for k in ids.keys():
        print("{} has {} files".format(k, len(ids[k])))

    return ids, files


def get_filename(image_id, image_type):
    """
    Method to get image file path from its id and type
    """
    global TRAIN_DATA, TEST_DATA, ADDITIONAL_DATA
    if image_type == "Type_1" or image_type == "Type_2" or image_type == "Type_3":
        data_path = os.path.join(TRAIN_DATA, image_type)
    elif image_type == "Test":
        data_path = TEST_DATA
    elif image_type == "AType_1" or image_type == "AType_2" or image_type == "AType_3":
        data_path = os.path.join(ADDITIONAL_DATA, image_type[1:])
    else:
        raise Exception("Image type '%s' is not recognized" % image_type)

    ext = "jpeg"
    return os.path.join(data_path, "{}.{}".format(image_id, ext))


def process_input(f, id_list, type_id, excluded_list):
    # process one type of input
    for i in id_list:
        filename = get_filename(i, type_id)
        if filename in excluded_list:
            continue
        img = cv2.imread(filename)
        done, x0, x1, y0, y1 = define_ROI(img)
        if done:
            f.close()
            sys.exit()
        f.write("{}\t{}\t{}\t{}\t{}\n".format(filename, x0, x1, y0, y1))
        print("--- target window ---")
        print("Starting point is {}, {}\n".format(x0, y0))
        print("Ending   point is {}, {}\n".format(x1, y1))


def parse_input():
    parser = argparse.ArgumentParser(description="labeling bounding box")
    parser.add_argument(
        "-t", "--type", action="store", help="specify the type you want to work on"
    )
    args = parser.parse_args()
    return args


def main():
    download_inputdata()
    ids, files = load_thumbnails()
    parse_input()
    t = int(args.type)
    print("type is " + str(t))
    # allows you to resume
    processed = set()
    os.makedirs("../output", exist_ok=True)
    out = OUTPUT_SUMMARY.format(t)
    if not os.path.isfile(out):
        sp.check_output(["touch", out])
    with open(out, "r") as f:
        lines = f.readlines()
        for l in lines:
            tokens = l.split("\t")
            processed.add(tokens[0])
    print(
        "these {} files has already being processed last time\n".format(len(processed))
    )
    for i in processed:
        print(i)

    # loops through all files
    with open(out, "a") as f:
        id_list = ids["train_type_" + str(t)]
        type_info = "Type_" + str(t)
        process_input(f, id_list, type_info, processed)


if __name__ == "__main__":
    main()
